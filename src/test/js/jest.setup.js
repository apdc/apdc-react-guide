import { configure } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

configure({ adapter: new Adapter() });

const xhrMockClass = () => ({
  open: jest.fn(),
  send: jest.fn(),
  setRequestHeader: jest.fn(),
});

jest.mock('shortid', () => ({
  generate: jest.fn(() => 1),
}));

global.XMLHttpRequest = jest.fn(xhrMockClass);
