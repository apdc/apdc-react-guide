import React from 'react';
import PropTypes from 'prop-types';
import { SpotlightManager } from '@atlaskit/onboarding';

class Guide extends React.Component {
  render() {
    return <SpotlightManager>{this.props.children}</SpotlightManager>;
  }
}

Guide.defaultProps = {
  children: <React.Fragment />,
};

Guide.propTypes = {
  children: PropTypes.any,
};

export default Guide;
