import React from 'react';
import { SpotlightTransition } from '@atlaskit/onboarding';
import {
  getAllSpotlightsFromUnseenGuides,
  getDataFromSpotlightReactComponent,
  getInitValue,
} from '../util/guide.util';

export default class GuideManager {
  constructor(reactComponent, guides, guideRepository) {
    this.reactComponent = reactComponent;
    this.guideRepository = guideRepository;
    this.versions = [];
    this.spotlights = [];

    this.back = this.back.bind(this);
    this.next = this.next.bind(this);
    this.finish = this.finish.bind(this);
    this.remindLater = this.remindLater.bind(this);

    this.functions = {
      next: this.next,
      finish: this.finish,
      back: this.back,
    };

    this.setup(guides);

    this.onChange = () => {};
    this.onFinish = () => {};
    this.onStart = () => {};
    this.started = false;
    this.finished = false;
  }

  setOnChange(func) {
    this.onChange = func;
  }

  setOnFinish(func) {
    this.onFinish = func;
  }

  setOnStart(func) {
    this.onStart = func;
  }

  enableGuideStarted() {
    if (this.spotlights.length > 0) {
      this.started = true;
    } else {
      this.finish();
    }
  }

  setup(guides) {
    this.initializeActiveSpotlightsState();

    return this.guideRepository
      .getAllOfVersionsOfSeenGuides()
      .then(seenGuideVersions => {
        const unseenGuides = getAllSpotlightsFromUnseenGuides(
          seenGuideVersions,
          guides,
        );
        const { spotlights, versions } = unseenGuides;

        this.spotlights = this.addMethodsToSpotlights(spotlights);
        this.versions = versions;

        this.enableGuideStarted();
        this.setSpotlightNumberOnStore(getInitValue(spotlights));
        return seenGuideVersions;
      })
      .catch(console.warn);
  }

  isStateNoInitialized() {
    return !this.reactComponent.state;
  }

  initializeActiveSpotlightsState() {
    const value = null;
    const { reactComponent } = this;

    if (this.isStateNoInitialized()) {
      reactComponent.state = {
        activeSpotlight: value,
      };

      return;
    }

    reactComponent.setState(state => ({ ...state, activeSpotlight: value }));
  }

  setSpotlightNumberOnStore(initValue) {
    this.reactComponent.setState(state => ({
      ...state,
      activeSpotlight: initValue,
    }));
  }

  next() {
    const { activeSpotlight } = this.reactComponent.state;
    const nextSpotlight = activeSpotlight + 1;

    this.reactComponent.setState(state => ({
      ...state,
      activeSpotlight: nextSpotlight,
    }));

    this.onChange(
      nextSpotlight,
      getDataFromSpotlightReactComponent(this.spotlights[nextSpotlight]),
      true,
    );
  }

  back() {
    const { activeSpotlight } = this.reactComponent.state;
    const previousSpotlight = activeSpotlight - 1;

    this.reactComponent.setState(state => ({
      ...state,
      activeSpotlight: previousSpotlight,
    }));

    this.onChange(
      previousSpotlight,
      getDataFromSpotlightReactComponent(this.spotlights[previousSpotlight]),
      false,
    );
  }

  finish() {
    this.reactComponent.setState(state => ({
      ...state,
      activeSpotlight: null,
    }));

    this.onFinish();
    this.finished = true;
    this.guideRepository.saveVersionsOfSeenGuides(this.versions);
  }

  remindLater() {
    this.reactComponent.setState(state => ({
      ...state,
      activeSpotlight: null,
    }));

    this.finished = true;
    this.onFinish();

  }

  renderSpotlight() {
    const { activeSpotlight } = this.reactComponent.state;

    if (this.started) {
      this.started = false;
      this.onStart(
        getDataFromSpotlightReactComponent(this.spotlights[activeSpotlight]),
      );
    }

    return this.finished ? (
      <React.Fragment />
    ) : (
      <SpotlightTransition>
        {this.spotlights[activeSpotlight]}
      </SpotlightTransition>
    );
  }

  mapStrToFunctions(actions) {
    return actions.map(action => {
      const onClick = this.functions[action.onClick];

      if (!onClick) {
        console.error(
          `Unrecognized guide function by name: ${action.onClick}.`,
        );
      }

      return {
        ...action,
        onClick,
      };
    });
  }

  addMethodsToSpotlights(spotlights) {
    return spotlights.map(spotlight => ({
      ...spotlight,
      props: {
        ...spotlight.props,
        actions: this.mapStrToFunctions(spotlight.props.actions),
      },
    }));
  }
}
