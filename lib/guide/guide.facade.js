"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime/helpers/classCallCheck"));

var _createClass2 = _interopRequireDefault(require("@babel/runtime/helpers/createClass"));

var _guide = _interopRequireDefault(require("./manager/guide.manager"));

(function () {
  var enterModule = typeof reactHotLoaderGlobal !== 'undefined' ? reactHotLoaderGlobal.enterModule : undefined;
  enterModule && enterModule(module);
})();

var __signature__ = typeof reactHotLoaderGlobal !== 'undefined' ? reactHotLoaderGlobal.default.signature : function (a) {
  return a;
};

var GuideFacade = /*#__PURE__*/function () {
  function GuideFacade(reactComponent, guides, guideRepository) {
    (0, _classCallCheck2.default)(this, GuideFacade);
    this.guideManager = new _guide.default(reactComponent, guides, guideRepository);
  }

  (0, _createClass2.default)(GuideFacade, [{
    key: "onChange",
    value: function onChange(func) {
      this.guideManager.setOnChange(func);
    }
  }, {
    key: "onFinish",
    value: function onFinish(func) {
      this.guideManager.setOnFinish(func);
    }
  }, {
    key: "onStart",
    value: function onStart(func) {
      this.guideManager.setOnStart(func);
    }
  }, {
    key: "getSpotlights",
    value: function getSpotlights() {
      return this.guideManager.spotlights;
    }
  }, {
    key: "renderGuideTour",
    value: function renderGuideTour() {
      return this.guideManager.renderSpotlight();
    }
  }, {
    key: "__reactstandin__regenerateByEval",
    // @ts-ignore
    value: function __reactstandin__regenerateByEval(key, code) {
      // @ts-ignore
      this[key] = eval(code);
    }
  }]);
  return GuideFacade;
}();

exports.default = GuideFacade;
;

(function () {
  var reactHotLoader = typeof reactHotLoaderGlobal !== 'undefined' ? reactHotLoaderGlobal.default : undefined;

  if (!reactHotLoader) {
    return;
  }

  reactHotLoader.register(GuideFacade, "GuideFacade", "D:\\projects\\apdc-react-guide\\src\\main\\js\\guide\\guide.facade.jsx");
})();

;

(function () {
  var leaveModule = typeof reactHotLoaderGlobal !== 'undefined' ? reactHotLoaderGlobal.leaveModule : undefined;
  leaveModule && leaveModule(module);
})();