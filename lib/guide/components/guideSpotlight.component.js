"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime/helpers/classCallCheck"));

var _createClass2 = _interopRequireDefault(require("@babel/runtime/helpers/createClass"));

var _possibleConstructorReturn2 = _interopRequireDefault(require("@babel/runtime/helpers/possibleConstructorReturn"));

var _getPrototypeOf2 = _interopRequireDefault(require("@babel/runtime/helpers/getPrototypeOf"));

var _inherits2 = _interopRequireDefault(require("@babel/runtime/helpers/inherits"));

var _react = _interopRequireDefault(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _onboarding = require("@atlaskit/onboarding");

(function () {
  var enterModule = typeof reactHotLoaderGlobal !== 'undefined' ? reactHotLoaderGlobal.enterModule : undefined;
  enterModule && enterModule(module);
})();

function _createSuper(Derived) { return function () { var Super = (0, _getPrototypeOf2.default)(Derived), result; if (_isNativeReflectConstruct()) { var NewTarget = (0, _getPrototypeOf2.default)(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return (0, _possibleConstructorReturn2.default)(this, result); }; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Date.prototype.toString.call(Reflect.construct(Date, [], function () {})); return true; } catch (e) { return false; } }

var __signature__ = typeof reactHotLoaderGlobal !== 'undefined' ? reactHotLoaderGlobal.default.signature : function (a) {
  return a;
};

var GuideSpotlight = /*#__PURE__*/function (_React$Component) {
  (0, _inherits2.default)(GuideSpotlight, _React$Component);

  var _super = _createSuper(GuideSpotlight);

  function GuideSpotlight() {
    (0, _classCallCheck2.default)(this, GuideSpotlight);
    return _super.apply(this, arguments);
  }

  (0, _createClass2.default)(GuideSpotlight, [{
    key: "render",
    value: function render() {
      var _this$props = this.props,
          target = _this$props.target,
          actions = _this$props.actions,
          header = _this$props.header,
          position = _this$props.position,
          content = _this$props.content,
          width = _this$props.width;
      return /*#__PURE__*/_react.default.createElement(_onboarding.Spotlight, {
        target: target,
        actions: actions,
        heading: header,
        width: width,
        dialogPlacement: position
      }, content);
    }
  }, {
    key: "__reactstandin__regenerateByEval",
    // @ts-ignore
    value: function __reactstandin__regenerateByEval(key, code) {
      // @ts-ignore
      this[key] = eval(code);
    }
  }]);
  return GuideSpotlight;
}(_react.default.Component);

GuideSpotlight.defaultProps = {
  actions: [],
  header: '',
  width: 400,
  position: 'bottom center'
};
GuideSpotlight.propTypes = {
  target: _propTypes.default.string.isRequired,
  // eslint-disable-next-line react/forbid-prop-types
  actions: _propTypes.default.array,
  header: _propTypes.default.string,
  position: _propTypes.default.string,
  content: _propTypes.default.string.isRequired,
  width: _propTypes.default.number
};
var _default = GuideSpotlight;
var _default2 = _default;
exports.default = _default2;
;

(function () {
  var reactHotLoader = typeof reactHotLoaderGlobal !== 'undefined' ? reactHotLoaderGlobal.default : undefined;

  if (!reactHotLoader) {
    return;
  }

  reactHotLoader.register(GuideSpotlight, "GuideSpotlight", "D:\\projects\\apdc-react-guide\\src\\main\\js\\guide\\components\\guideSpotlight.component.jsx");
  reactHotLoader.register(_default, "default", "D:\\projects\\apdc-react-guide\\src\\main\\js\\guide\\components\\guideSpotlight.component.jsx");
})();

;

(function () {
  var leaveModule = typeof reactHotLoaderGlobal !== 'undefined' ? reactHotLoaderGlobal.leaveModule : undefined;
  leaveModule && leaveModule(module);
})();