"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime/helpers/classCallCheck"));

var _createClass2 = _interopRequireDefault(require("@babel/runtime/helpers/createClass"));

var _guide = require("./util/guide.util");

var _guide2 = _interopRequireDefault(require("./guide.facade"));

(function () {
  var enterModule = typeof reactHotLoaderGlobal !== 'undefined' ? reactHotLoaderGlobal.enterModule : undefined;
  enterModule && enterModule(module);
})();

var __signature__ = typeof reactHotLoaderGlobal !== 'undefined' ? reactHotLoaderGlobal.default.signature : function (a) {
  return a;
};

var GuideFactory = /*#__PURE__*/function () {
  function GuideFactory() {
    (0, _classCallCheck2.default)(this, GuideFactory);
  }

  (0, _createClass2.default)(GuideFactory, null, [{
    key: "createGuideManagerWithGuideSchemes",
    value: function createGuideManagerWithGuideSchemes(reactComponent, guidesSchemes, guideRepository) {
      var guides = (0, _guide.mapGuideSchemesToReactComponents)(guidesSchemes);
      return new _guide2.default(reactComponent, guides, guideRepository);
    }
  }, {
    key: "createGuideManager",
    value: function createGuideManager(reactComponents, guides, guideRepository) {
      return new _guide2.default(reactComponents, guides, guideRepository);
    }
  }]);
  return GuideFactory;
}();

exports.default = GuideFactory;
;

(function () {
  var reactHotLoader = typeof reactHotLoaderGlobal !== 'undefined' ? reactHotLoaderGlobal.default : undefined;

  if (!reactHotLoader) {
    return;
  }

  reactHotLoader.register(GuideFactory, "GuideFactory", "D:\\projects\\apdc-react-guide\\src\\main\\js\\guide\\guide.factory.js");
})();

;

(function () {
  var leaveModule = typeof reactHotLoaderGlobal !== 'undefined' ? reactHotLoaderGlobal.leaveModule : undefined;
  leaveModule && leaveModule(module);
})();